# Desctiption
Personal level manager - Don't Starve Together modification.

Personal level manager, for set level players. Only admins can set level.
Admin commands:
/addplayer [num] - add player in players list, [num] - index player in server (press "tab")
/delplayer [num] - delete player from players list, [num] - index player in list
/delallplayers - delete all players from players list
/setlevel [num] [level] - set level player, [num] - index player player in list, [level] - value number

Location stats data file:
..\client_save\personallevelmanager

Configuration options for dedicated server:
CLIENT_VISIBLECTRAFTTABS = true
or
CLIENT_VISIBLECTRAFTTABS = false

This is mod made special for personal use! please don't write reports.

[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1984522413)