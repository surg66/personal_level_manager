name = "Personal level manager"
description = [[Personal level manager, for set level players. Only admins can set level.
Admin commands:
/addplayer [num] - add player in players list, [num] - index player in server (press "tab")
/delplayer [num] - delete player from players list, [num] - index player in list
/delallplayers - delete all players from players list
/setlevel [num] [level] - set level player, [num] - index player player in list, [level] - value number

Location stats data file:
..\client_save\personallevelmanager

Configuration options for dedicated server:
CLIENT_VISIBLECTRAFTTABS = true
or
CLIENT_VISIBLECTRAFTTABS = false

This is mod made special for personal use! please don't write reports.]]
author = "surg, WellKery, 󰀍*No4ka* *󰀍"
version = "1.0.0"
forumthread = ""
api_version_dst = 10
icon_atlas = "modicon.xml"
icon = "modicon.tex"
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false
dst_compatible = true
all_clients_require_mod = true
client_only_mod = false
server_filter_tags = {""}
configuration_options =
{
	{
		name = "CLIENT_VISIBLECTRAFTTABS",
		label = "Client visible ctraft tabs",
		hover = "Sets client visible ctraft tabs",
        options = {
                    { description = "Disable", data = false },
                    { description = "Enable",  data = true }
                  },
		default = true,
	}
}