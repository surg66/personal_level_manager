local _G = GLOBAL
local require = GLOBAL.require
local TheNet = GLOBAL.TheNet

Assets =
{
    Asset("ATLAS", "images/personallevel.xml"),
	Asset("IMAGE", "images/personallevel.tex"),
}

local CLIENT_VISIBLECTRAFTTABS = GetModConfigData("CLIENT_VISIBLECTRAFTTABS")

local SelectedPlayersStatus = require "widgets/selectedplayersstatus"

local function NormalizeNumber(value)
    local str = _G.tostring(value)
    local res = ""
    for i = 1, #str do
        local code = _G.string.byte(str, i);
        -- [0..9] code 48..57
        if code >= 48 and code <= 57 then
            res = res.._G.string.char(code)
        end
    end
    local result = 0
    if res ~= "" then
        result = _G.tonumber(res)
    end
    return result
end

for _, v in ipairs({"forest_network", "cave_network"}) do
    AddPrefabPostInit(v, function(inst)
        inst:AddComponent("selectedplayerscollector")
    end)
end

AddPlayerPostInit(function(inst)
    inst:AddComponent("healthsyncer")
    inst:AddComponent("levelsyncer")
end)

AddClassPostConstruct("widgets/controls", function(self)
    self.selectedplayersstatus = self.topleft_root:AddChild(SelectedPlayersStatus(self.owner))
    self.selectedplayersstatus:SetPosition(120, -60, 0)
    self.item_notification:SetPosition(400, 150, 0) -- gift

    if not CLIENT_VISIBLECTRAFTTABS and _G.ThePlayer ~= nil and
       _G.ThePlayer.Network ~= nil and not _G.ThePlayer.Network:IsServerAdmin() then
        local original_ShowCraftingAndInventory = self.ShowCraftingAndInventory
        self.ShowCraftingAndInventory = function(self)
            original_ShowCraftingAndInventory(self)
            self.crafttabs:Hide()  
        end

        self.inst:DoTaskInTime(0, function()            
            self.selectedplayersstatus:SetPosition(60, -60, 0)
            self.crafttabs:Hide()
        end)
    end
end)

AddUserCommand("addplayer", {
    prettyname = "Add player", 
    desc = "Add player in list.", 
    permission = _G.COMMAND_PERMISSION.ADMIN,
    slash = true,
    usermenu = false,
    servermenu = false,
    params = {"num"},
    vote = false,
    serverfn = function(params, caller)
        if _G.TheWorld.net.components.selectedplayerscollector ~= nil then
            _G.TheWorld.net.components.selectedplayerscollector:TryAddPlayer(caller, NormalizeNumber(params.num))
        end
    end
})

AddUserCommand("delplayer", {
    prettyname = "Delete player", 
    desc = "Delete player from list.", 
    permission = _G.COMMAND_PERMISSION.ADMIN,
    slash = true,
    usermenu = false,
    servermenu = false,
    params = {"num"},
    vote = false,
    serverfn = function(params, caller)
        if _G.TheWorld.net.components.selectedplayerscollector ~= nil then
            _G.TheWorld.net.components.selectedplayerscollector:TryDeletePlayer(caller, NormalizeNumber(params.num))
        end
    end
})

AddUserCommand("delallplayers", {
    prettyname = "Delete player", 
    desc = "Delete all players from list.", 
    permission = _G.COMMAND_PERMISSION.ADMIN,
    slash = true,
    usermenu = false,
    servermenu = false,
    params = {},
    vote = false,
    serverfn = function(params, caller)
        if _G.TheWorld.net.components.selectedplayerscollector ~= nil then
            _G.TheWorld.net.components.selectedplayerscollector:DeleteAllPlayers(caller)
        end
    end
})

AddUserCommand("setlevel", {
    prettyname = "Set level", 
    desc = "Change level player.", 
    permission = _G.COMMAND_PERMISSION.ADMIN,
    slash = true,
    usermenu = false,
    servermenu = false,
    params = {"num", "level"},
    vote = false,
    serverfn = function(params, caller)
        if _G.TheWorld.net.components.selectedplayerscollector ~= nil then
            _G.TheWorld.net.components.selectedplayerscollector:TrySetLevel(caller, NormalizeNumber(params.num), NormalizeNumber(params.level))
        end
    end
})
