local function OnNewLevelDirty(inst)
    inst:PushEvent("clientnewleveldirty", { level = inst.components.levelsyncer._level:value() })
end

local function OnNewLevel(inst, data)
    local self = inst.components.levelsyncer
    if self._level:value() ~= data.newlevel then
        self._level:set(data.newlevel)
        OnNewLevelDirty(inst)
    end
end

local function InitServer(inst)
    local component = TheWorld.net.components.selectedplayerscollector
    OnNewLevel(inst, { newlevel = component and component:GetLevel(inst.userid) or 1 })
end

local LevelSyncer = Class(function(self, inst)
    self.inst = inst

    self._level = net_ushortint(inst.GUID, "levelsyncer._level", "newleveldirty") -- [0..65535]
    self._level:set(1)

    if TheWorld.ismastersim then
        inst:ListenForEvent("newlevel", OnNewLevel)
        inst:DoTaskInTime(0, InitServer)
    else
        inst:ListenForEvent("newleveldirty", OnNewLevelDirty)
        inst:DoTaskInTime(0, OnNewLevelDirty)
    end
end)

function LevelSyncer:GetLevel()
    return self._level:value()
end

return LevelSyncer
