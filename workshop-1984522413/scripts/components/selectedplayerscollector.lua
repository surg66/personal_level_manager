--------------------------------------------------------------------------
--[[ Selected Players Collector class definition ]]
--------------------------------------------------------------------------

return Class(function(self, inst)

--------------------------------------------------------------------------
--[[ Member variables ]]
--------------------------------------------------------------------------

--Public
self.inst = inst

--Private
local _world             = TheWorld
local _ismastersim       = _world.ismastersim
local _collectionplayers = {} -- client/server
local _serverdata        = nil

--Network
local _netvars =
{
    collectionplayers_json = net_string(inst.GUID, "selectedplayerscollector._netvars.collectionplayers_json", "collectionplayersjsondirty"),
}

--------------------------------------------------------------------------
--[[ Private member functions ]]
--------------------------------------------------------------------------

local function GetClients()
    local clients = TheNet:GetClientTable()

    for i, v in ipairs(clients) do
        if v.performance ~= nil and not TheNet:GetServerIsClientHosted() then
            table.remove(clients, i)
            break
        end
    end

    return clients
end

local function HasPlayerInCollection(userid)
    for _, v in ipairs(_collectionplayers) do
        if v.userid == userid then
            return true
        end
    end

    return false
end

local function LoadServerData()
    TheSim:GetPersistentString("personallevelmanager", function(load_success, data) 
        if load_success and data ~= nil then
            _serverdata = json.decode(data)
        end
    end)
end

local function UpdateServerData()
    if _serverdata ~= nil then
        TheSim:SetPersistentString("personallevelmanager", json.encode(_serverdata), false)
    end
end

local function SyncClientCollectionPlayers()
    -- salt need for call "set" and send new data, for sync
    local data = json.encode({ salt = GetTime(), list = _collectionplayers })

    _netvars.collectionplayers_json:set(data)
end

--------------------------------------------------------------------------
--[[ Private event handlers ]]
--------------------------------------------------------------------------

local function OnCollectionPlayersJsonDirty()
    local data = json.decode(_netvars.collectionplayers_json:value())

    if data ~= nil and data.list ~= nil then
        _collectionplayers = {}
        for _, v in ipairs(data.list) do
            table.insert(_collectionplayers, { userid = v.userid, name = v.name })
        end
    end
end

local function OnPlayerJoined(world, player)
    SyncClientCollectionPlayers()
    -- sync level after join
    player:PushEvent("newlevel", { newlevel = self:GetLevel(player.userid) })
end

--------------------------------------------------------------------------
--[[ Initialization ]]
--------------------------------------------------------------------------

_netvars.collectionplayers_json:set("{}")

if _ismastersim then
    LoadServerData()
    self.inst:ListenForEvent("ms_playerjoined", OnPlayerJoined, _world)
else
    self.inst:ListenForEvent("collectionplayersjsondirty", OnCollectionPlayersJsonDirty)
end

--------------------------------------------------------------------------
--[[ Public member functions ]]
--------------------------------------------------------------------------

function self:TryAddPlayer(caller, num)
    if _ismastersim and caller ~= nil and caller.Network ~= nil and caller.Network:IsServerAdmin() then
        local index = 1
        for _, v in ipairs(GetClients()) do
            if index == num and not HasPlayerInCollection(v.userid) then
                table.insert(_collectionplayers, { userid = v.userid, name = v.name })
                SyncClientCollectionPlayers()
                print("[PersonalLevelManager] "..caller.name.."("..caller.userid..") add "..v.name.."("..v.userid..") to collection")
                break
            end
            index = index + 1
        end
    end
end

function self:TryDeletePlayer(caller, num)
    if _ismastersim and caller ~= nil and caller.Network ~= nil and caller.Network:IsServerAdmin() then
        for i, v in ipairs(_collectionplayers) do
            if i == num then
                table.remove(_collectionplayers, i)
                SyncClientCollectionPlayers()
                print("[PersonalLevelManager] "..caller.name.."("..caller.userid..") delete "..v.name.."("..v.userid..") to collection")
                break
            end
        end
    end
end

function self:DeleteAllPlayers(caller)
    if _ismastersim and caller ~= nil and caller.Network ~= nil and caller.Network:IsServerAdmin() then
        _collectionplayers = {}
        SyncClientCollectionPlayers()
        print("[PersonalLevelManager] "..caller.name.."("..caller.userid..") clear collection")
    end
end

function self:TrySetLevel(caller, num, level)
    if _ismastersim and caller ~= nil and caller.Network ~= nil and caller.Network:IsServerAdmin() then
        for i, v in ipairs(_collectionplayers) do
            if i == num then
                for _, pv in ipairs(AllPlayers) do
                    if v.userid == pv.userid then
                        pv:PushEvent("newlevel", { newlevel = level })

                        if _serverdata == nil then
                            _serverdata = {}
                        end

                        _serverdata[v.userid] = level

                        UpdateServerData()
                        print("[PersonalLevelManager] "..caller.name.."("..caller.userid..") set level "..level.." to "..v.name.."("..v.userid..") to collection")
                        break
                    end
                end
                break
            end
        end
    end
end

function self:GetList()
    return _collectionplayers
end

function self:GetLevel(userid)
    if _ismastersim and _serverdata ~= nil then
        for k, v in pairs(_serverdata) do
            if k == userid then
                return v
            end
        end
    end

    return 1
end

--------------------------------------------------------------------------
--[[ Save/Load ]]
--------------------------------------------------------------------------

function self:OnSave()
    local data =
    {
        collectionplayers = _collectionplayers,
    }

    return data
end

function self:OnLoad(data)
    if data ~= nil and data.collectionplayers ~= nil then
        _collectionplayers = {}

        for _, v in ipairs(data.collectionplayers) do
            table.insert(_collectionplayers, { userid = v.userid, name = v.name })
        end

        SyncClientCollectionPlayers()
    end
end

--------------------------------------------------------------------------
--[[ End ]]
--------------------------------------------------------------------------

end)
