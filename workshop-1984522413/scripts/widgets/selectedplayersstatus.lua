local Widget = require "widgets/widget"
local PlayerStatusBadge = require "widgets/playerstatusbadge"

local SelectedPlayersStatus = Class(Widget, function(self, owner)
    Widget._ctor(self, "SelectedPlayersStatus", owner)

    self.healthbars = {}
    self:SetScale(.8)
    self:OnUpdate(0)
    self:StartUpdating()
end)

function SelectedPlayersStatus:OnUpdate(dt)
    local componentcollector = TheWorld.net.components.selectedplayerscollector
    local selectedplayers = componentcollector and componentcollector:GetList() or {}
    local player_listing = {}
    for _, selectedplayer in ipairs(selectedplayers) do
        local player = nil
        local name = selectedplayer.name
        for _, pv in ipairs(AllPlayers) do
            if selectedplayer.userid == pv.userid then
                player = pv
                name = pv.name
                break
            end
        end

        table.insert(player_listing, {player = player, name = name})
    end

    local respositioning = false

    while #self.healthbars > #player_listing do
        self.healthbars[#self.healthbars]:Kill()
        table.remove(self.healthbars, #self.healthbars)
        respositioning = true
    end

    while #self.healthbars < #player_listing do
        table.insert(self.healthbars, self:AddChild(PlayerStatusBadge(self.owner)))
        respositioning = true
    end

    for i, bar in ipairs(self.healthbars) do
        if bar.player ~= player_listing[i].player then
            bar:SetPlayer(player_listing[i].player, player_listing[i].name)
        elseif bar.player == nil and not bar.isnilplayer then
            bar:SetPlayer(nil, player_listing[i].name)
        end
        if respositioning then
            bar:SetIndex(i)
        end
    end

    if respositioning == true then
        self:RespostionBars()
    end
end

function SelectedPlayersStatus:RespostionBars()
    local spacing = -110

    for i, bar in ipairs(self.healthbars) do
        bar:SetPosition(0, spacing * (i -1))
    end
end

return SelectedPlayersStatus
