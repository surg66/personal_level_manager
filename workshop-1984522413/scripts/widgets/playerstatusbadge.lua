local Badge = require "widgets/badge"
local UIAnim = require "widgets/uianim"
local Text = require "widgets/text"
local Image = require "widgets/image"
local Widget = require "widgets/widget"
local HealthBadge = require "widgets/healthbadge"

local COLORS = 
{
    {165/255, 165/255, 165/255, 1},
    {141/255, 179/255, 226/255, 1},
    {  0/255, 112/255, 192/255, 1},
    {146/255, 208/255,  80/255, 1},
    {  0/255, 176/255,  80/255, 1},
    {227/255, 108/255,  10/255, 1},
    {217/255, 149/255, 148/255, 1},
    {192/255,   0/255,   0/255, 1},
    { 98/255,  36/255,  35/255, 1},
    {112/255,  48/255, 160/255, 1}
}

local function SetPlayerName(self, playername, playercolour)
    self.name_root:SetPosition(12, 0)

    local name = (playername ~= nil and #playername > 0) and playername or STRINGS.UI.SERVERADMINSCREEN.UNKNOWN_USER_NAME

    self.playername:SetTruncatedString(name, 300, 35, "...")

    local name_left = self.name_banner_left_width - 45

    local text_w = math.max(self.name_banner_center_width, self.playername:GetRegionSize())
    self.playername:SetPosition(text_w/2 + name_left, 0)
    self.playername:SetColour(playercolour)

    local banner_right_offset = -10
    
    self.name_banner_center:SetPosition(name_left, 0)
    self.name_banner_center:SetScale((text_w + banner_right_offset) / self.name_banner_center_width, 1)
    
    self.name_banner_right:SetPosition(name_left + text_w + banner_right_offset - 3, 0)
end

local PlayerStatusBadge = Class(Badge, function(self, owner)
    Badge._ctor(self, nil, owner, { 174 / 255, 21 / 255, 21 / 255, 1 }, "status_health")

    self:SetClickable(false)

    self.isnilplayer = false

    self.index = self:AddChild(Text(CHATFONT_OUTLINE, 30))
    self.index:SetColour({.57, .57, .57, .57})
    self.index:SetString("")
    self.index:SetPosition(-50, 0)
    
    if ThePlayer ~= nil and ThePlayer.Network ~= nil and not ThePlayer.Network:IsServerAdmin() then
        self.index:Hide()
    end

    self.rank_root = self:AddChild(Widget("rankroot"))
    self.rank_root:MoveToBack()
    self.rank_root.bg = self.rank_root:AddChild(Image("images/personallevel.xml", "bgrank.tex"))
    self.rank_root.bg:SetScale(0.7)
    self.rank_root.bg:SetTint(unpack(COLORS[1]))
    self.rank_root.num = self.rank_root:AddChild(Text(CHATFONT_OUTLINE, 30))
    self.rank_root.num:SetColour(UICOLOURS.WHITE)
    self.rank_root.num:SetString("")
    self.rank_root.num:SetPosition(2, -3)
    
    self.rank_root:SetPosition(0, -48)
    
    self.name_root = self:AddChild(Widget("nameroot"))
    self.name_root:MoveToBack()
    self.name_root:SetScale(1.1)

    self.arrow = self.underNumber:AddChild(UIAnim())
    self.arrow:GetAnimState():SetBank("sanity_arrow")
    self.arrow:GetAnimState():SetBuild("sanity_arrow")
    self.arrow:GetAnimState():PlayAnimation("neutral")
    self.arrow:SetScale(0.85)

    self.name_banner_center = self.name_root:AddChild(Image("images/lavaarena_hud.xml", "username_banner_filler.tex"))
    self.name_banner_center:SetHRegPoint(ANCHOR_LEFT)
    self.name_banner_center_width = self.name_banner_center:GetSize()
    self.name_banner_left = self.name_root:AddChild(Image("images/lavaarena_hud.xml", "username_banner_start.tex"))
    self.name_banner_left:SetHRegPoint(ANCHOR_LEFT)
    self.name_banner_left_width = self.name_banner_left:GetSize()
    self.name_banner_right = self.name_root:AddChild(Image("images/lavaarena_hud.xml", "username_banner_end.tex"))
    self.name_banner_right:SetHRegPoint(ANCHOR_LEFT)
    self.name_banner_right_width = self.name_banner_right:GetSize()

    self.playername = self.name_root:AddChild(Text(CHATFONT_OUTLINE, 30))

    self._onclienthealthdirty = function(src, data) self:SetPercent(data.percent) end
    self._onclienthealthstatusdirty = function() self:RefreshStatus() end
    self._onclientnewleveldirty = function(src, data) self:SetLevel(data.level) end

    -- hack for combine status
    if self.bg ~= nil then
        self.bg:Hide()
    end

    if self.num ~= nil then
        self.num:Hide()
    end
end)

function PlayerStatusBadge:SetPlayer(player, namenil)
    if self.player ~= nil and self.player ~= player then
        self.inst:RemoveEventCallback("clienthealthdirty", self._onclienthealthdirty, self.player)
        self.inst:RemoveEventCallback("clienthealthstatusdirty", self._onclienthealthstatusdirty, self.player)
        self.inst:RemoveEventCallback("clientnewleveldirty", self._onclientnewleveldirty, self.player)
    end
    
    self.player = player

    if self.player ~= nil then 
        self.userid = player.userid
        self.isnilplayer = false

        self.inst:ListenForEvent("clienthealthdirty", self._onclienthealthdirty, player)
        self.inst:ListenForEvent("clienthealthstatusdirty", self._onclienthealthstatusdirty, player)
        self.inst:ListenForEvent("clientnewleveldirty", self._onclientnewleveldirty, player)

        self.arrowdir = 0

        SetPlayerName(self, player.name, player.playercolour)

        if player.components.healthsyncer ~= nil then
            self.percent = player.components.healthsyncer:GetPercent()
            self:SetPercent(self.percent)
        end

        if player.components.levelsyncer ~= nil then
            self.level = player.components.levelsyncer:GetLevel()
            self:SetLevel(self.level)
        end

        self.backing:GetAnimState():SetMultColour(1, 1, 1, 1)
        self.anim:Show()
        self.circleframe:GetAnimState():OverrideSymbol("icon", "status_health", "icon")
        self.rank_root:Show()
    else
        self.isnilplayer = true
        self.backing:GetAnimState():SetMultColour(0.1, 0.1, 0.1, 1)
        self.anim:Hide()
        self.circleframe:GetAnimState():ClearOverrideSymbol("icon")
        self.rank_root:Hide()
        SetPlayerName(self, namenil, { 1, 1, 1, 1 })
    end
end

function PlayerStatusBadge:SetIndex(index)
    self.index:SetString(tostring(index)..".")
end

function PlayerStatusBadge:SetLevel(val)
    if val < 0 then
        val = 1
    end

    if val > #COLORS - 1 then
        self.rank_root.bg:SetTint(unpack(COLORS[10]))
    else
        self.rank_root.bg:SetTint(unpack(COLORS[val]))
    end

    self.rank_root.num:SetString(tostring(val))
end

function PlayerStatusBadge:SetPercent(val)
    val = val == 0 and 0 or math.max(val, 0.001)

    if self.percent < val then
        if self.arrowdir <= 0 then
            self:PulseGreen()
        end
    elseif self.percent > val then
        if self.arrowdir >= 0 then
            self:PulseRed()
        end
    end

    Badge.SetPercent(self, val)

    self:RefreshStatus()
end

function PlayerStatusBadge:RefreshStatus()
    local arrowdir = self.player.components.healthsyncer ~= nil and self.player.components.healthsyncer:GetOverTime() or 0

    if self.arrowdir ~= arrowdir then
        self.arrowdir = arrowdir

        self.arrow:GetAnimState():PlayAnimation((arrowdir > 1 and "arrow_loop_increase_most") or
                                                    (arrowdir < 0 and "arrow_loop_decrease_most") or
                                                    "neutral", true)
    end

    local warning = (arrowdir > 1 and {0,1,0,1}) or
                    ((arrowdir < 0 or (self.percent <= .33 and self.percent > 0)) and {1,0,0,1}) or
                    nil

    if warning ~= nil then
        self:StartWarning(unpack(warning))
    else
        self:StopWarning()
    end
end

return PlayerStatusBadge
